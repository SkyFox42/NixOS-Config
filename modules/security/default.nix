{config, pkgs, lib, ...}:

with lib; {
	# Prevent replacing the running kernel w/o reboot
	security.protectKernelImage = mkDefault true;
}
