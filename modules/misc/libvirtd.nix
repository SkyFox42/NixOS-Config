{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	virtualisation.libvirtd.enable = mkDefault (builtins.elem "libvirtd" roles);
}
