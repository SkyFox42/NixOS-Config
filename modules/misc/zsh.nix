{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	programs.zsh = mkIf (builtins.elem "zsh" roles) {
		enable = mkDefault true;
		ohMyZsh = {
			enable = mkDefault true;
			plugins = [ "git" ];
			theme = mkDefault "clean";
			customPkgs = with pkgs; [
				nix-zsh-completions
			];
		};
		shellInit = mkIf (builtins.elem "home-manager" roles) ''
			. "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
		'';
	};
}
