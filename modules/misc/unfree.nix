{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) ([
		"samsung-UnifiedLinuxDriver"
	] ++ (if (builtins.elem "nvidia" roles) then [
		"nvidia-x11"
		"nvidia-settings"
	] else []));
}
