{config, pkgs, lib, ...}:

{
	imports = [
		./zsh.nix
		./fox-user.nix
		./syncthing.nix
		./libvirtd.nix
		./ssh-agent.nix
		./unfree.nix
		./term-utils.nix
		./starship.nix
		./kde-connect.nix
	];
}
