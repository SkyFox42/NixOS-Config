{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	programs.starship = let
		langStyle = "bold fg:#D597CE";
	in mkIf (builtins.elem "term-utils" roles) {
		enable = true;
		settings = {
			format = lib.concatStrings [
				"$username"
				"$localip"
				"[ |](fg:#86BBD8)"
				"$directory"
				"$git_branch"
				"$git_commit"
				"$git_state"
				"$git_status"
				"$package"
				"$c"
				"$cmake"
				"$dart"
				"$golang"
				"$haskell"
				"$kotlin"
				"$nodejs"
				"$python"
				"$rust"
				"$zig"
				"$nix_shell"
				"$docker_context"
				"$cmd_duration"
				"$line_break"
				"$character"
				"$status"
			];
			username = {
				show_always = true;
				style_user = "bold fg:#86BBD8";
				style_root = "bold fg:#DA627D";
				format = "[$user]($style)";
			};
			localip = {
				style = "bold fg:#33658A";
				format = "[@$localipv4]($style)";
				disabled = false;
			};
			directory = {
				style = "bold fg:#86BBD8";
				format = "[ $path ]($style)";
				truncation_length = 3;
				truncation_symbol = "…/";
				read_only = " ";
				substitutions = {
					Documents = "";
					Downloads = "";
					Music = "";
					Pictures = "";
				};
			};
			git_branch = {
				symbol = " ";
				style = "bold fg:#A267AC";
				format = "\\[[$symbol $branch]($style)\\]";
			};
			git_commit = {
				tag_symbol = " 🏷️ ";
				style = "bold fg:#BFB051";
				format = "\\[[$hash$tag]($style)\\]";
			};
			git_state = {
				format = "[\\[$state $progress_current/$progress_total\\]]($style) ";
			};
			git_status = {
				style = "bold fg:#DA627D";
				format = "[\\[$all_status$ahead_behind\\]]($style)";
			};
			package = {
				symbol = "📦️ ";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			c = {
				symbol = "C";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			docker_context = {
				symbol = "Docker";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style) $path\\]";
			};
			cmake = {
				symbol = "Cmake";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			dart = {
				symbol = "Dart";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			golang = {
				symbol = "Go";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			haskell = {
				symbol = "Haskell";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			kotlin = {
				symbol = "Kotlin";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			nodejs = {
				symbol = "JS";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			python = {
				symbol = "Py";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			rust = {
				symbol = "Rust";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			zig = {
				symbol = "Zig";
				style = langStyle;
				format = "\\[[$symbol ($version)]($style)\\]";
			};
			nix_shell = {
				symbol = "Nix";
				style = langStyle;
				format = "\\[[$symbol $state]($style)\\]";
			};
			cmd_duration = {
				style = "bold fg:#F0F696";
				format = "\\[[羽 $duration]($style)\\]";
			};
		};
	};
}
