{ config, pkgs, lib, unstable, ... }:

with lib; let
	roles = config.fox.roles;

	unstable-pkgs = unstable.legacyPackages."${pkgs.system}";
in {
	config = mkIf (builtins.elem "term-utils" roles) {
		environment.systemPackages = with pkgs; [
			zellij
			unstable-pkgs.nushell
			libarchive
			fd
			ripgrep
			bat
			du-dust
			tealdeer
			ranger
			htop
		];

		environment.etc."ranger/rc.conf".text = ''
			set vcs_aware true
			set cd_tab_case insensetive
			set line_numbers relative
			set one_indexed true
			set wrap_scroll true
		'';

		programs.neovim = {
			enable = true;
			configure = {
				customRC = ''
					set lines relativenumber
					set backspace=indent,eol,start
					syntax on
				'';
				packages.myVimPackage = with pkgs.vimPlugins; {
					start = [ vim-nix ];
					opt = [];
				};
			};
			viAlias = true;
			vimAlias = true;
			defaultEditor = true;
		};
	};
}
