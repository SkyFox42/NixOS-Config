{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "ssh-agent" roles) {
		programs.ssh.askPassword = mkForce "${pkgs.lxqt.lxqt-openssh-askpass}/bin/lxqt-openssh-askpass";

		programs.ssh.startAgent = mkDefault true;
	};
}
