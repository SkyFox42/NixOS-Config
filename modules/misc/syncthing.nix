{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "syncthing" roles) {
		# Open Syncthing ports
		networking.firewall.allowedTCPPorts = [ 22000 ];
		networking.firewall.allowedUDPPorts = [ 21027 ];
	};
}
