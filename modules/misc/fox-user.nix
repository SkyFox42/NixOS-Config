{ config, pkgs, lib, ... }:

with lib;
let
	dockerCfg = config.virtualisation.docker;
	libvirtdCfg = config.virtualisation.libvirtd;

	roles = config.fox.roles;
in {
	users.users.fox = mkIf (builtins.elem "fox-user" roles) {
		isNormalUser = true;
		shell = mkIf (builtins.elem "zsh" roles) "/run/current-system/sw/bin/zsh";
		extraGroups = [
			(mkIf libvirtdCfg.enable "libvirtd")
			(mkIf dockerCfg.enable "docker")
		];
	};
}
