{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "kde-connect" roles) {
		# Open KDE Connect ports
		networking.firewall.allowedTCPPortRanges = [ { from = 1714; to = 1764; } ];
		networking.firewall.allowedUDPPortRanges = [ { from = 1714; to = 1764; } ];
	};
}
