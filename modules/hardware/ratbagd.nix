{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "ratbagd" roles) {
		services.ratbagd.enable = true;
	};
}
