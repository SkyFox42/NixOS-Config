{config, pkgs, lib, ...}:

{
	imports = [
		./nvidia.nix
		./touchpad.nix
		./bluetooth-headset.nix
		./ratbagd.nix
	];
}
