{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "touchpad" roles) {
		# Enable touchpad support.
		services.xserver.libinput.enable = mkDefault true;
	};
}
