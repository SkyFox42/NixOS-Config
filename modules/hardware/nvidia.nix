{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;

	nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
		export __NV_PRIME_RENDER_OFFLOAD=1
		export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
		export __GLX_VENDOR_LIBRARY_NAME=nvidia
		export __VK_LAYER_NV_optimus=NVIDIA_only
		exec -a "$0" "$@"
	'';
in {

	config = mkIf (builtins.elem "nvidia" roles) {
		# Enable the proprietary Nvidia driver :(
		services.xserver.videoDrivers = [ "modesetting" "nvidia" ];
		hardware.nvidia.modesetting.enable = mkDefault true;

		environment.systemPackages = mkIf (builtins.elem "nvidia-prime" roles) [ nvidia-offload ];

		hardware.nvidia.prime = mkIf (builtins.elem "nvidia-prime" roles) {
			offload.enable = mkForce true;

			intelBusId = mkDefault "PCI:0:2:0";

			nvidiaBusId = mkDefault "PCI:1:0:0";
		};
	};
}
