{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "bluetooth-headset" roles) {
		hardware.bluetooth.enable = mkDefault true;
		hardware.bluetooth.disabledPlugins = ["sap"];

		environment.systemPackages = [
			pkgs.libspatialaudio
		];

		environment.etc = {
			"wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
bluez_monitor.properties = {
	["bluez5.enable-sbc-xq"] = true,
	["bluez5.enable-msbc"] = true,
	["bluez5.enable-hw-volume"] = true,
	["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
}
			'';
		};
	};
}
