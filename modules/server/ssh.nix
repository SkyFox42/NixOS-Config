{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;

	sshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMutqaQ7DrBAIS0hP8YRg+QP5wlzPXj4GRym6oJ7hb4s";
in {
	config = mkIf (builtins.elem "sshd" roles) {
		programs.ssh = {
			hostKeyAlgorithms = mkDefault ["ssh-ed25519"];
			pubkeyAcceptedKeyTypes = mkDefault ["ssh-ed25519"];
		};

		# Enable the OpenSSH daemon.
		services.openssh = {
			enable = mkDefault true;
			hostKeys = mkDefault [
				{
					path = "/etc/ssh/ssh_host_ed25519_key";
					type = "ed25519";
				}
			];
			extraConfig = ''
				AllowTcpForwarding no
				X11Forwarding no
				AllowAgentForwarding no
				AllowStreamLocalForwarding no
				AuthenticationMethods publickey
			'';
			knownHosts = {
				SkyFox = {
					hostNames = [ "host" ];
					publicKey = sshKey;
				};
			};
			settings = {
				PasswordAuthentication = mkDefault false;
				KbdInteractiveAuthentication = mkDefault false;
			};
		};

		users.users.root.openssh.authorizedKeys.keys = [
			sshKey
		];
		users.users.fox = mkIf (builtins.elem "fox-user" roles) {
			openssh.authorizedKeys.keys = [
				sshKey
			];
		};

		security.pam.enableSSHAgentAuth = true;
	};
}
