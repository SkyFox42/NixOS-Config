{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	imports = [
		./ssh.nix
	];
}
