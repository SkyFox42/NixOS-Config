{config, pkgs, lib, nixpkgs, ...}:

with lib; let
	foxcfg = config.fox;
in {
	imports = [
		./boot
		./misc
		./hardware
		./desktop
		./server
		./laptop
	];

	options = {
		fox = {
			roles = mkOption {
				type = types.nonEmptyListOf types.str;
				default = [""];
				description = ''
					List of roles for this system. Used to enable modules.
					Available roles are:
						- "gnome" - Enable Gnome
						- "xfce" - Enable XFCE
						- "uefi-grub" - Enable UEFI support and install GRUB
						- "uefi-systemd" - Enable UEFI support and install systemd-boot
						- "bios" - Use BIOS and GRUB for boot
						- "FDE" - Enable Full Disk Encryption
						- "fox-user" - Enable non-root user "fox"
						- "zsh" - Enable Zsh with oh-my-zsh and set as default for "fox"
						- "desktop" - Enable common options for desktop
						- "laptop" - Enable common options for laptop
						- "localtime" - Enable setting timezone via geoclue2
						- "touchpad" - Enable touchpad support
						- "nvidia" - Install proprietary Nvidia drivers
						- "nvidia-prime" - Enable Nvidia Prime
						- "syncthing" - Open default Syncthing ports
						- "home-manager" - Enable home-manager
						- "libvirtd" - Enable libvirtd
						- "printing" - Enable CUPS and install some drivers
						- "scan" - Enable SANE and install some drivers
						- "sound" - Enable sound with PipeWire (also enables ALSA and Pulse emulation in PipeWire)
						- "security" - Enable some security options (only 1 at the moment)
						- "sshd" - Enable and configure the SSH daemon
						- "ssh-agent" - Enable OpenSSH agent
						- "kde-connect" - Open ports for KDE Connect
						- "term-utils" - Install useful CLI/TUI utilities
						- "sway" - Enable Sway compositor
						- "bluetooth-headset" - Add configuration for bluetooth headsets
						- "ratbagd" - Enable ratbagd for mouse configuration
				'';
			};
		};
	};

	config = {
		networking.hostName = "host"; # Define the hostname.

		# Select internationalisation properties.
		i18n.defaultLocale = "en_GB.UTF-8";
		console = {
			font = "Lat2-Terminus16";
			keyMap = "us";
		};

		environment.systemPackages = with pkgs; [
			# Terminal utils
			wget
			openssl
			xdg-user-dirs
		];

		# Enable Git
		programs.git.enable = mkDefault true;

		# Disable sudo
		security.sudo.enable = mkDefault false;

		# Enable passwd
		users.mutableUsers = mkDefault true;

		nix = {
			registry.nixpkgs = {
				exact = true;
				from = {
					type = "indirect";
					id = "nixpkgs";
				};
				flake = nixpkgs;
			};
			nixPath = [ "nixpkgs=flake:nixpkgs" ];
			settings = {
				auto-optimise-store = true;
				experimental-features = [ "nix-command" "flakes" "ca-derivations" ];
				keep-outputs = true;
				keep-env-derivations = true;
				keep-derivations = true;
				max-jobs = "auto";
				substituters = [ "https://cache.ngi0.nixos.org/" ];
				trusted-public-keys = [ "cache.ngi0.nixos.org-1:KqH5CBLNSyX184S9BKZJo1LxrxJ9ltnY2uAs5c/f1MA=" ];
			};
		};
	};
}
