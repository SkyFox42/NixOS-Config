{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "laptop" roles) {
		services.power-profiles-daemon.enable = mkDefault true;

		hardware.bluetooth.enable = mkDefault true;

		services.upower.enable = mkDefault true;
	};
}
