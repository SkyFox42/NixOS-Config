{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "localtime" roles) {
		services.localtime.enable = mkDefault true;

		services.geoclue2 = {
			enable3G = mkDefault false;
			enableModemGPS = mkDefault false;
		};
	};
}
