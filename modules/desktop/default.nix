{config, pkgs, lib, ...}:

with lib; let
	desktopOverlay = self: super: {
		vaapiIntel = super.vaapiIntel.override { enableHybridCodec = true; };
	};

	roles = config.fox.roles;
in {
	imports = [
		./printing.nix
		./sound.nix
		./gnome.nix
		./xfce.nix
		./localtime.nix
		./scan.nix
		./sway.nix
	];

	config = mkIf (builtins.elem "desktop" roles) {
		nixpkgs.overlays = [ desktopOverlay ];

		boot.kernelModules = [ "fuse" ];
		boot.supportedFilesystems = [ "ntfs" ];

		# nixpkgs.config.allowUnfree = true;

		networking.networkmanager.wifi.macAddress = mkIf config.networking.networkmanager.enable "random";
		networking.networkmanager.ethernet.macAddress = mkIf config.networking.networkmanager.enable "random";

		services.avahi.enable = mkDefault true;

		# Enable the X11 windowing system.
		services.xserver.enable = mkDefault true;
		services.xserver.xkb.layout = "us";

		environment.systemPackages = with pkgs; [
			glib
		];

		# All the fonts!
		fonts.packages = with pkgs; [
			twitter-color-emoji
			# nerdfonts
			(nerdfonts.override {
				fonts = [
					"CascadiaCode"
					"Iosevka"
					"IosevkaTerm"
				];
			})
			emacs-all-the-icons-fonts
			source-sans
			source-serif
		];

		fonts.fontconfig.defaultFonts = {
			emoji = [
				"Twitter Color Emoji"
				"Noto Color Emoji"
			];

			serif = [
				"Source Serif 4"
				"DejaVu Serif"
			];

			sansSerif = [
				"Source Sans 3"
				"DejaVu Sans"
			];

			monospace = [
				"Iosevka Nerd Font"
				"CaskaydiaCove Nerd Font"
				"DejaVu Sans Mono"
			];
		};

		# Enable accelerated video playback
		hardware.opengl = {
			enable = mkDefault true;
			driSupport = mkDefault true;
			driSupport32Bit = mkDefault true;
			extraPackages = with pkgs; [
				intel-media-driver # LIBVA_DRIVER_NAME=iHD
				vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
				vaapiVdpau
				libvdpau-va-gl
			];
		};
	};
}
