{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "scan" roles) {
		hardware.sane = {
			enable = mkDefault true;

			extraBackends = [
				pkgs.hplip
			];
		};

		services.saned.enable = mkDefault true;
	};
}
