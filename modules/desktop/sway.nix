{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;

	gnomeEnabled = builtins.elem "gnome" roles;
in {
	config = mkIf (builtins.elem "sway" roles) {
		environment.systemPackages = with pkgs; [
			qt5.qtwayland
		];

		services.xserver.displayManager.gdm.enable = mkDefault true;
		services.xserver.displayManager.gdm.wayland = mkDefault true;

		services.xserver.updateDbusEnvironment = mkDefault true;

		services.colord.enable = mkDefault true;

		xdg.portal = {
			enable = mkDefault true;
			extraPortals = mkIf (!gnomeEnabled) [
				pkgs.xdg-desktop-portal-gtk
			];
		};

		qt.platformTheme = mkDefault "gtk";

		networking.networkmanager.enable = mkDefault true;

		services.gvfs = {
			enable = true;
			package = pkgs.gnome.gvfs;
		};

		programs.sway = {
			enable = true;
			wrapperFeatures = {
				gtk = true;
			};
			extraSessionCommands = ''
				# SDL:
				export SDL_VIDEODRIVER=wayland
				# QT (needs qt5.qtwayland in systemPackages):
				export QT_QPA_PLATFORM=wayland-egl
				export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
				export QT_WAYLAND_FORCE_DPI=physical
				# Firefox
				export MOZ_ENABLE_WAYLAND=1
				# Elementary/EFL
				export ECORE_EVAS_ENGINE=wayland_egl
				export ELM_ENGINE=wayland_egl
				# Java AWT
				export _JAVA_AWT_WM_NONREPARENTING=1
				# SSH agent socket
				export SSH_AUTH_SOCK=/run/user/1000/ssh-agent
			'';
			extraPackages = with pkgs; [
				kitty brightnessctl eww-wayland wofi
				swaylock swayidle swaybg pamixer
				udiskie gnome.eog
			];
			extraOptions = [];
		};
	};
}
