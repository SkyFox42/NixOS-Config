{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "sound" roles) {
		# Enable sound.
		sound.enable = mkDefault true;
		services.pipewire.enable = mkDefault true;
		services.pipewire.pulse.enable = mkDefault true;
		services.pipewire.alsa.enable = mkDefault true;
		hardware.pulseaudio.enable = mkOverride 950 false;
	};
}
