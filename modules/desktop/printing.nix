{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "printing" roles) {
		services.system-config-printer.enable = mkDefault true;

		# Enable CUPS to print documents.
		services.printing = {
			enable = mkDefault true;
			drivers = with pkgs; [
				samsung-unified-linux-driver
				hplip
			];
		};
	};
}
