{config, pkgs, lib, ...}:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "gnome" roles) {
		# Enable Gnome
		services.xserver.displayManager.gdm.enable = mkDefault true;
		services.xserver.displayManager.gdm.wayland = mkDefault true;
		services.xserver.desktopManager.gnome.enable = mkDefault true;
		services.gnome.gnome-keyring.enable = mkForce false;
		environment.gnome.excludePackages = with pkgs.gnome; [
			pkgs.gnome-tour
			epiphany
			geary
			gnome-maps
			gnome-music
			pkgs.gnome-photos
			gnome-calendar
			gnome-weather
			accerciser
			totem
			tali
			iagno
			hitori
			atomix
			gnome-contacts
			gnome-initial-setup
		];

		environment.systemPackages = with pkgs; [
			qt5.qtwayland
			gnome.gnome-tweaks
			gnomeExtensions.appindicator
			gnomeExtensions.gsconnect
		];

		services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];

		qt.platformTheme = "gnome";

		xdg.portal = {
			enable = mkDefault true;
		};
	};
}
