{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	config = mkIf (builtins.elem "xfce" roles) {
		# Enable the XFCE Desktop Environment
		services.xserver.desktopManager.xfce.enable = mkDefault true;
		services.xserver.displayManager.defaultSession = mkIf
			(lists.all (x: !x) [(builtins.elem "gnome" roles)]) "xfce";
	};
}
