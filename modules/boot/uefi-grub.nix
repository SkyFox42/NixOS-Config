{ config, lib, pkgs, ... }:

with lib;
let
	roles = config.fox.roles;

	uefi-grub = builtins.elem "uefi-grub" roles;

	encryptedBoot = builtins.elem "encrypted-boot" roles;
in {
	config = mkIf uefi-grub {
		boot.loader.efi.efiSysMountPoint =
			mkDefault (if encryptedBoot then "/boot/efi" else "/boot");

		boot.loader.efi.canTouchEfiVariables = mkDefault true;
		boot.loader.grub = {
			enable = mkDefault true;
			device = mkDefault "nodev"; # or "nodev" for efi only
			efiSupport = mkDefault true;
		};
	};
}
