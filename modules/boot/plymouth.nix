{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;

	adi1090x-plymouth = pkgs.callPackage ../../packages/adi1090x/plymouth-themes {};
in {
	config = mkIf (builtins.elem "plymouth" roles) {
		systemd.services.plymouth-quit.serviceConfig.ExecStartPre = "${pkgs.busybox}/bin/sleep 5";

		boot.plymouth = {
			enable = true;
			theme = "connect";
			themePackages = [ adi1090x-plymouth ];
		};
	};
}
