{ config, lib, pkgs, ... }:

with lib;
let
	roles = config.fox.roles;

	uefi-systemd = builtins.elem "uefi-systemd" roles;

	encryptedBoot = builtins.elem "encrypted-boot" roles;
in {
	config = mkIf uefi-systemd {
		boot.loader.efi.efiSysMountPoint =
			mkDefault (if encryptedBoot then "/boot/efi" else "/boot");

		boot.loader.efi.canTouchEfiVariables = mkDefault true;
		boot.loader.systemd-boot = {
			enable = mkDefault true;
			editor = mkDefault false;
		};
	};
}
