{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;

	encryptedBoot = builtins.elem "encrypted-boot" roles;
in {
	config = mkIf (builtins.elem "FDE" roles) {
		boot.loader.grub.enableCryptodisk = true;

		boot.initrd = {
			luks.devices."root" = {
				device = "/dev/disk/by-uuid/root-dev-uuid";
				preLVM = true;
				keyFile = mkIf encryptedBoot "/keyfile.bin";
			};
			secrets = {
				"keyfile.bin" = mkIf encryptedBoot "/etc/secrets/initrd/keyfile.bin";
			};
		};
	};
}
