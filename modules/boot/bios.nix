{ config, pkgs, lib, ... }:

with lib; let
	roles = config.fox.roles;
in {
	boot.loader.grub = mkIf (builtins.elem "bios" roles) {
		enable = mkDefault true;
		device = mkDefault "/dev/sda"; # or "nodev" for efi only
	};
}
