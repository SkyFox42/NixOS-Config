{config, pkgs, lib, ...}:

{
	imports = [
		./bios.nix
		./FDE.nix
		./uefi-grub.nix
		./uefi-systemd.nix
		./plymouth.nix
	];

	boot.tmp.cleanOnBoot = lib.mkDefault (!config.boot.tmp.useTmpfs);
}
