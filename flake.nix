{
	inputs = {
		nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
		unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

		home-manager = {
			url = "github:nix-community/home-manager/release-23.11";
			inputs.nixpkgs.follows = "nixpkgs";
		};

		nixos-generators = {
			url = "github:nix-community/nixos-generators";
			inputs.nixpkgs.follows = "nixpkgs";
		};

		disko = {
			url = "github:nix-community/disko/v1.6.1";
			inputs.nixpkgs.follows = "nixpkgs";
		};
	};

	outputs = { self, nixpkgs, home-manager, nixos-generators, disko, ... }@attrs: {
			devShells.x86_64-linux.default = nixpkgs.legacyPackages.x86_64-linux.mkShell {
				nativeBuildInputs = [
					disko.packages."x86_64-linux".default
				];
			};
			packages.x86_64-linux = {
				installer = nixos-generators.nixosGenerate {
					system = "x86_64-linux";
					specialArgs = attrs;
					format = "install-iso";
					modules = [
						./modules/fox.nix
						(
							{ config, pkgs, lib, ... }: {
								config.fox.roles = [
									"term-utils"
								];
							}
						)
						(
							{ config, pkgs, lib, ... }: {
								environment.etc = {
									ideapad-system.source = self.nixosConfigurations.ideapad.config.system.build.toplevel;
									matebook-system.source = self.nixosConfigurations.matebook.config.system.build.toplevel;
									system-flake.source = self;
									"system-flake-inputs.json".text = builtins.toJSON (builtins.mapAttrs (name: value: "${value}") attrs);
									disk-configs.source = ./disk-configs;
								};

								environment.systemPackages = [
									disko.packages."x86_64-linux".default
								];

								security.sudo.enable = true;

								users.mutableUsers = false;

								isoImage.isoName = lib.mkForce "nixos.iso";
							}
						)
					];
				};
			};
			nixosConfigurations = {
				matebook = nixpkgs.lib.nixosSystem {
					system = "x86_64-linux";
					specialArgs = attrs;
					modules = [
						home-manager.nixosModules.home-manager
						{
							home-manager.useGlobalPkgs = true;
							home-manager.useUserPackages = true;
						}
						./device/matebook.nix
						./configuration.nix
						(
							{config, pkgs, lib, ...}: {
								fox.roles = [
									"gnome" "uefi-grub" "FDE" "fox-user" "desktop" "touchpad"
									"nvidia" "nvidia-prime" "syncthing" "home-manager"
									"libvirtd" "printing" "scan" "sound" "security" "ssh-agent"
									"bluetooth-headset" "term-utils" "kde-connect" "laptop"
									"ratbagd" "encrypted-boot"
								];

								system.stateVersion = "22.05";
							}
						)
					];
				};
				ideapad = nixpkgs.lib.nixosSystem {
					system = "x86_64-linux";
					specialArgs = attrs;
					modules = [
						home-manager.nixosModules.home-manager
						{
							home-manager.useGlobalPkgs = true;
							home-manager.useUserPackages = true;
						}
						./device/ideapad.nix
						./configuration.nix
						(
							{config, pkgs, lib, ...}: {
								fox.roles = [
									"uefi-systemd" "FDE" "fox-user" "desktop" "touchpad"
									"syncthing" "home-manager" "libvirtd" "printing" "scan" "sound"
									"security" "ssh-agent" "bluetooth-headset" "term-utils" "sway"
									"kde-connect" "laptop" "ratbagd"
								];

								system.stateVersion = "23.05";
							}
						)
					];
				};
				kvm-guest = nixpkgs.lib.nixosSystem {
					system = "x86_64-linux";
					specialArgs = attrs;
					modules = [
						./device/kvm.nix
						./configuration.nix
					];
				};
				this-system = nixpkgs.lib.nixosSystem {
					system = "x86_64-linux";
					specialArgs = attrs;
					modules = [
						./configuration.nix
					];
				};
			};
		};
}
