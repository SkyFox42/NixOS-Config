{config, pkgs, lib, ...}:

{
	imports = [
		./hardware-configuration.nix # Include the results of the hardware scan

		./modules/fox.nix

		./localConfig.nix
	];

	# The global useDHCP flag is deprecated, therefore explicitly set to false here.
	# Per-interface useDHCP will be mandatory in the future, so this generated config
	# replicates the default behaviour.
	networking.useDHCP = false;
}
