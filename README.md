# NixOS-Config

My configuration files for NixOS.

## Some notes

- "A hashed password can be generated using `mkpasswd -m sha-512` after installing the mkpasswd package." - NixOS manual
