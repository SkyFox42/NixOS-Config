{ config, pkgs, ... }:

{
	networking.interfaces.eth0.useDHCP = true;
}
