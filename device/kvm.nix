{ config, pkgs, ... }:

{
	networking.interfaces.enp1s0.useDHCP = true;
	networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];
}
