{ config, pkgs, lib, ... }:

with lib; let
	battery-conservation-on = pkgs.writeShellScriptBin "battery-conservation-on.sh" ''
		echo 1 > /sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/conservation_mode
	'';

	battery-conservation-off = pkgs.writeShellScriptBin "battery-conservation-off.sh" ''
		echo 0 > /sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/conservation_mode
	'';

	get-battery-conservation = pkgs.writeShellScriptBin "get-battery-conservation.sh" ''
		cat /sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/conservation_mode
	'';
in {
	environment.systemPackages = [
		battery-conservation-on
		battery-conservation-off
		get-battery-conservation
	];

	boot.kernelPackages = mkDefault pkgs.linuxPackages_latest;

	networking.interfaces.wlo1.useDHCP = true;

	boot.kernel.sysctl."vm.swappiness" = 35;
	zramSwap = {
		enable = true;
		memoryPercent = 100;
	};

	# Enable CPU microcode updates
	hardware.cpu.amd.updateMicrocode = mkDefault true;

	# Enable fstrim
	services.fstrim.enable = mkDefault true;
}
