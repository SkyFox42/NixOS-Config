{ config, pkgs, lib, ... }:

with lib; {
	networking.interfaces.wlp0s20f3.useDHCP = true;

	boot.kernel.sysctl."vm.swappiness" = 1;

	# Needed for the system to detect headphones
	environment.etc."modprobe.d/sound.conf".text = ''
		options snd_hda_intel model=dell-headset-multi
	'';

	# Enable CPU microcode updates
	hardware.cpu.intel.updateMicrocode = mkDefault true;

	# Enable fstrim
	services.fstrim.enable = mkDefault true;
}
