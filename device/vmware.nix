{ config, pkgs, ... }:

{
	networking.interfaces.ens33.useDHCP = true;

	environment.systemPackages = with pkgs; [
		open-vm-tools
		vmfs-tools
	];
}
