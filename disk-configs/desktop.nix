{ disks ? { main-drive = "/dev/nvme0n1"; }, ... }: {
	disko.devices = {
		disk = {
			main-drive = {
				type = "disk";
				device = disks.main-drive;
				content = {
					type = "table";
					format = "gpt";
					partitions = [
						{
							name = "ESP";
							start = "1MiB";
							end = "512MiB";
							bootable = true;
							content = {
								type = "filesystem";
								format = "vfat";
								mountpoint = "/boot";
								mountOptions = [
									"defaults"
								];
							};
						}
						{
							name = "luks";
							start = "512MiB";
							end = "100%";
							content = {
								type = "luks";
								name = "root";
								extraOpenArgs = ["--allow-discards"];
								settings.keyFile = "/tmp/secret.key";
								content = {
									type = "lvm_pv";
									vg = "vg";
								};
							};
						}
					];
				};
			};
		};
		lvm_vg = {
			vg = {
				type = "lvm_vg";
				lvs = {
					swap = {
						size = "24G";
						content = {
							type = "swap";
						};
					};
					system = {
						size = "100%FREE";
						content = {
							type = "filesystem";
							format = "ext4";
							mountpoint = "/";
						};
					};
				};
			};
		};
	};
}
